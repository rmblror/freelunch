FROM python:3.8-slim-buster

RUN mkdir /app

COPY Pipfile* /app/
WORKDIR /app

RUN apt-get update && apt-get install -y texlive-base texlive-latex-extra latexmk && \
    pip3 install pipenv && pipenv install --ignore-pipfile
    
