# freelunch

freelunch is a free software project to keep track of recipes and nutrition in a well-formatted cookbook.
It is designed to be highly modular. Users can add/change recipes, LaTeX templates, and even (eventually) sources of nutrition information.
Recipes are stored as yml files, where preparation steps and ingredients are listed, and this software handles the rest.
See latex/main.pdf for an example with my rugbrød recipe.

# Goals

The aim of this project is to have a standardized way for storing and sharing recipes.
With myriad websites offering printable forms of their recipes and (sometimes sparse or absent) nutrition information, one must manually copy each recipe into their own system in order to have a unified repository.
Instead, recipes could be shared simply as a list of ingredients and preparation steps, and each user can seamlessly add them to their own cookbook with whatever information they wish automatically derived from the ingredients.

This software could enable the creation of a public recipe repository as a substitute for the common practice of accessing recipes via ad-ridden user-tracking sites.
The repository could be community-organized, with volunteer maintainers packaging collections of user-submitted recipes in a format that could be easily distributed for use with this software as a frontend.
Being designed modularly, the community would be free to develop other frontends and to extend features of this project.

I was motivated in the first place by wanting to optimize my normal weekday meals around cost and preparation time per nutritional value. I'm also interested in developing a plant-based diet that's high in protein with a satisfactory amino acid profile and answering whether it is close to the optimum of cost and time, which requires more sophisticated analysis tools than I could find freely.

# Features to add

- Recipe sorting (e.g. by protein/cost, vitamins/kcal, etc.)
- Mobile-friendly LaTeX templates
- Nutrition information for week-long diet plans
- Support for imperial units (though you really ought not use imperial units.)
- Potentially other sources of nutrition data
- If less tech-savvy users are intimidated (and this proves useful to enough people), I could eventually add a GUI for creating the recipes and generating the cookbooks.

# Dependencies

## Docker

I highly recommend using docker in order to ensure that you have the same runtime environment as I developed the software in. This should also allow it to work in a cross-platform way, though I don't have access to a Windows machine for testing.

### GNU/Linux

In a GNU/Linux environment, you can install Docker through your normal package manager, e.g.

``sudo apt-get install docker''

Then you must start the docker service

``sudo systemctl start docker''

and optionally configure your user to be able to run docker without elevated privilege

``sudo groupadd docker
sudo usermod -aG docker $USER''

With this done, you can create the docker image by running setup.sh

``./setup.sh''

### Windows

First install Docker Desktop https://docs.docker.com/docker-for-windows/install/ and run it.

In the Docker settings (available via the right-click menu in the icon in the system tray while Docker is running), under "Shared Drives", make sure that the drive where freelunch is stored is checked. This allows the docker container to read and write from these files.

There may be issues with Docker on Windows requiring certain virtualization settings to be enabled in the BIOS. If you encounter such a problem, please let me know, since I can't test it for myself on my machine. In any case, this is an easily solved problem in general, so don't let it dissuade you.

Open a powershell in the root directory of freelunch and run setup.psi to create the docker image.

## Handling dependencies yourself

If you already use LaTeX and have it installed in your system, then you may prefer to handle the python dependencies on your own to avoid wasting 1GB or so of disk space.

You can use pipenv to set up the python environment by calling

``pipenv install''

in the root directory of this repository. 

# Use

Use recipes/example.yml as a guide to constructing recipe files, and store them all in recipes/.
Refer to them by their filenames without the extension in the profile.

Either modify profiles/default.yml or copy it to e.g. profiles/myprofile.yml and modify freelunch.py to use 'myprofile' instead. A more elegant solution to this will follow.

**NB: you must use spaces, not tabs, in the yml files. Most good text editors can be configured to do this automatically.**

You can interactively find ingredients in the frida database by running
```
./runinteractive.sh
pipenv run python -i frida.py
```
```python
search('milk')
```
Then when it returns a list of entries containing the word 'milk', write the index of the entry you prefer into the profile using a text editor of your choice.
The per kilogram price you'd like it to consider in the price calculation of the recipes it's used in should also be written here, possibly along with the density and rules for other ways it can be referred to (e.g. 'cloves' of garlic). Examples for this are included in the current default.yml

The 'make ingredients' flag can also be used to append a page to the document for every ingredient listed from the database, showing the data it uses in the recipes. This can be a quick way to troubleshoot in case something doesn't seem to add up -- in particular, the amino acid data is incomplete for several entries.

To create the pdf from the profile and recipe files, run
```
./run.sh
```
Or on Windows, call run.psi inside the powershell.

# Recipes

You can check out https://www.gitlab.com/rmblror/fl_local to see the recipes I've currently uploaded. I recommend including your own recipes, templates, and profiles in the directory fl_local as I've done here, so that this repository can be updated with minimal issues. A more elegant solution than storing recipes in another git repository will follow.

# Amino acid plots

The amino acid plots currently show the absolute amount of the essential amino acids per gram protein via the bar length, and the relative fraction of the essential amino acids that each acid comprises in the recipe/ingredient relative to the reference via the color. The other amino acids can be shown by uncommenting them in the profile, and there are also settings to toggle whether the amino acid plots are shown for main recipes, dependency recipes, and ingredients.

# Known issues

The density of the various ingredients is not given in the database I'm using. Therefore, any ingredient which has a density appreciably different from water (1g/mL) which you wish to include volumentrically must have its density specified in the profile. Otherwise, it will assume 1g/mL without a warning.
*NB: Measuring flour by volume hinders sharing of recipes, because density varies quite a lot, and it is the relative mass of ingredients that determines baking outcomes*

When calculating nutrition and cost per weight, this assumes that the weight of the final result is equal to the sum of the weight of the ingredients.
This holds roughly true when baking, but when one makes rice for example, the weight of the water isn't typically counted as an ingredient, and when it is, the amount that evaporates away is certainly missed. I'm not sure how best to handle that, but just bear this in mind when interpreting the results.

# Database

The database I've currently implemented is from the National Food Institute at the Technical University of Denmark and can be downloaded [here](https://frida.fooddata.dk/?lang=en).
Though it is free, public data, it is not yet licensed in such a way that users can redistribute it, though it is possible that it will become so.
For now, you must download it and move the english ods spreadsheet to frida/frida-en.ods.
I've been in contact with the database maintainers who indicated that there will likely be an API from around December that will be remotely accessible at runtime of the code, meaning that manually downloading it won't be necessary from then.

This database is curated in Denmark with ingredients that reflect what is commonly found here.
It lacks some common ingredients and is not perfect for international use, but it does have detailed information about the amino acid profiles for each food, which is very rare in such databases.
What I'll most likely add in the near term is a way to supplement frida with other data. One could manually copy an ingredient or two from a more fleshed-out proprietary database like nutritiondata.self.com, but I won't get into the legal issues of distributing that for now.

Food data (frida.fooddata.dk), version 4, 2019, National Food Institute, Technical University of Denmark
Food data made available by National Food Institute, Technical University of Denmark (frida.fooddata.dk)

